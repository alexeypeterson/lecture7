package sbp.db;

import org.springframework.stereotype.Service;
import sbp.JDBC.Person;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class PersonDao {

    private final String LOCAL_URL = "jdbc:sqlite:F:\\JDSber\\DB\\JDSber.db";


    /**
     * Добавляем пользователя Person в базу
     *
     */
    public boolean createPerson(Person person){

        final String createPersonPreparedStatement = "insert into Person(name, city, age) values(?,?,?)";
        boolean result = false;

        try (Connection connection = DriverManager.getConnection(LOCAL_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(createPersonPreparedStatement))
        {
            preparedStatement.setString(1,person.getName());
            preparedStatement.setString(2,person.getCity());
            preparedStatement.setInt(3,person.getAge());
            preparedStatement.executeUpdate();
            result = true;

        }catch (SQLException ex){
            ex.printStackTrace();

        }
        return result;
    }

    /**
     * Выборка всех пользователей из таблицы Person
     * @return Collections.emptyList() если таблица пустая
     *
     */
    public List<Person> readAllPerson() {
        final String readAllPersonQuery = "select * from Person";

        try (Connection connection = DriverManager.getConnection(LOCAL_URL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(readAllPersonQuery)) {
            List<Person> personList = new ArrayList<>();

            while (resultSet.next()) {
                Person localPerson = new Person(
                        resultSet.getString("name"),
                        resultSet.getString("city"),
                        resultSet.getInt("age"));
                personList.add(localPerson);
            }
            return personList;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }


    /**
     * Удаление записи по id пользователя
     */
    public boolean deletePerson(int id){

        final String createPersonPreparedStatement = "delete from Person where id = ?";
        boolean result = false;

        try (Connection connection = DriverManager.getConnection(LOCAL_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(createPersonPreparedStatement))
            {
                preparedStatement.setInt(1,id);
                preparedStatement.executeUpdate();
                result = true;
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        return result;
    }

    /**
     * Выборка всей информации по пользователю из таблицы Person
     * @return  Collections.emptyList() если таблица пустая
     */
    public List<Person> readPersonByName(String name) {
    final String readPersonByNameQuery = "select * from Person where name = ?";

        try (Connection connection = DriverManager.getConnection(LOCAL_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(readPersonByNameQuery)) {
             List<Person> personList = new ArrayList<>();

             preparedStatement.setString(1, name);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Person localPerson = new Person(
                        resultSet.getString("name"),
                        resultSet.getString("city"),
                        resultSet.getInt("age"));
                personList.add(localPerson);
            }
            return personList;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     *
     * Выборка все информации по городу из таблицы Person
     * @return  Collections.emptyList() если таблица пустая
     *
     */
    public List<Person> readPersonByCity(String city) {
        final String readPersonByNameQuery = "select * from Person where city = ?";

        try (Connection connection = DriverManager.getConnection(LOCAL_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(readPersonByNameQuery)) {
            List<Person> personList = new ArrayList<>();

            preparedStatement.setString(1, city);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Person localPerson = new Person(
                        resultSet.getString("name"),
                        resultSet.getString("city"),
                        resultSet.getInt("age"));
                personList.add(localPerson);
            }
            return personList;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Обновление пользователя с определенным id
     * @param id - ID пользователя для обновления в таблице Person
     * @param name - имя пользователя
     * @param city - город
     * @param age - возраст
     * @return true если обновление успешно выполнено
     */
    public boolean updatePersonById(int id, String name, String city, int age){

        final String updatePersonByIdQuery = "update Person set name = ?, city = ?, age = ? where id = ?";
        boolean result = false;

        try(Connection connection = DriverManager.getConnection(LOCAL_URL);
            PreparedStatement preparedStatement = connection.prepareStatement(updatePersonByIdQuery)){

            preparedStatement.setString(1, name);
            preparedStatement.setString(2, city);
            preparedStatement.setInt(3, age);
            preparedStatement.setInt(4, id);
            preparedStatement.executeUpdate();
            result = true;

        }catch(SQLException ex){
            ex.printStackTrace();
        }

        return result;

    }

    /**
     * Получение ID пользователя по его параметрам
     * @param name - имя пользователя
     * @param city - город
     * @param age - возраст
     * @return - ID из таблицы Person
     */
    public int getIDbyPerson(String name, String city, int age){

        final String getIDbyPersonQuery = "Select id from Person where name = ? and city = ? and age = ?";
        int resultID = 0;

        try(Connection connection = DriverManager.getConnection(LOCAL_URL);
            PreparedStatement preparedStatement = connection.prepareStatement(getIDbyPersonQuery)){

            preparedStatement.setString(1, name);
            preparedStatement.setString(2, city);
            preparedStatement.setInt(3, age);
            preparedStatement.executeQuery();
            resultID = preparedStatement.executeQuery().getInt(1);

        }catch(SQLException ex){
            ex.printStackTrace();
        }

        return resultID;
    }



}
