package sbp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sbp.JDBC.Person;
import sbp.db.PersonDao;

import java.util.List;

@RestController
public class ClientController {
    private final PersonDao dao;

    @Autowired
    public ClientController(PersonDao dao) {
        this.dao = dao;
    }

    /**
     * Создание записи в таблице Person
     */
    @PostMapping(value = "/persons")
    public ResponseEntity<?> createPerson(@RequestBody Person person){
        dao.createPerson(person);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Выборка всех пользователей из таблицы Person
     * @return List<Person>
     */
    @GetMapping(value = "/persons")
    public ResponseEntity<List<Person>> readPerson() {
        final List<Person> personList = dao.readAllPerson();

        return personList != null &&  !personList.isEmpty()
                ? new ResponseEntity<>(personList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Выборка всей информации по пользователям из таблицы Person по определенному имени
     * @param name - имя пользователя
     * @return List<Person>
     */
    @GetMapping(value = "/persons/{name}")
    public ResponseEntity<Person> readByName(@PathVariable(name = "name") String name) {
        final List<Person> personList = dao.readPersonByName(name);

        return personList != null && !personList.isEmpty()
                ? new ResponseEntity(personList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Внесение изменений в запись о пользователе по id
     * @param id - ID пользователя для обновления в таблице Person
     * @param name - имя пользователя
     * @param city - город
     * @param age - возраст
     * @return false, если изменения не внесены
     */
    @PutMapping(value = "/persons/{id}")
    public ResponseEntity<?> updateByName(@PathVariable(name = "id") int id,
                                        @RequestParam("name") String name,
                                        @RequestParam("city") String city,
                                        @RequestParam("age") int age) {
        final boolean updated = dao.updatePersonById(id, name, city, age);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    /**
     * Удаление записи о пользователе по id
     * @param id - id пользователя в таблице Person
     * @return - false, если удаление неудалось
     */
    @DeleteMapping(value = "/persons/{id}")
    public ResponseEntity<?> deleteByID(@PathVariable(name = "id") int id) {
        final boolean deleted = dao.deletePerson(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}
