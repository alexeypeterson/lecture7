package sbp.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MyIOExample {
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) {
        Path path = Paths.get(fileName);
        boolean result = false;

        if (Files.isDirectory(path)) {
            System.out.println("Aбсолютный путь: " + path.toAbsolutePath());
            System.out.println("Родительский путь: " + path.getParent());
        } else if (Files.isRegularFile(path)) {
            try {
                System.out.println("Размер файла: " + Files.size(path));
                System.out.println("Время последнего изменения: " + Files.getLastModifiedTime(path));
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            try {
                Files.createFile(path);
                result = true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }



    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     *  copy(Path source, Path target)
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        Path srcFileName = Paths.get(sourceFileName);
        Path dstFileName = Paths.get(destinationFileName);
        boolean result = false;


        try {
            Files.copy(srcFileName, dstFileName);
            result = true;
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     * copy(InputStream in, Path target)
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        Path srcFileName = Paths.get(sourceFileName);
        Path dstFileName = Paths.get(destinationFileName);
        boolean result = false;

       try{
          Files.copy(Files.newInputStream(srcFileName), dstFileName);
          result = true;
       }catch (IOException ex){
           ex.printStackTrace();
       }
        return result;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     * copy(Path source, OutputStream out)
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) {

        Path srcFileName = Paths.get(sourceFileName);
        Path dstFileName = Paths.get(destinationFileName);
        boolean result = false;
        Path pathCopy = null;

        if (Files.exists(srcFileName)) {
            try {
                List<String> list = Files.readAllLines(srcFileName);
                pathCopy = Files.write(Paths.get(destinationFileName), list);
                result = true;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
}
