
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;


class MyIOExampleTest {



    MyIOExample myIOExampleTest = new MyIOExample();

    /**
     * Проверка метода workWithFileTest
     */
    @Test
    void workWithFileTest() {
        boolean excepted = myIOExampleTest.workWithFile("MyTestFile");
        Assertions.assertTrue(excepted);
    }

    /**
     * Проверка метода copyFile
     */
    @Test
    void copyFileTest() {
        boolean excepted = myIOExampleTest.copyFile("ForCopyTest", "ForCopyTest_copy");
        Assertions.assertTrue(excepted);
    }

    /**
     * Проверка метода copyFile при попытке скопировать несуществующий файл
     */
    @Test
    void copyFileTest_Error() {
        boolean excepted = myIOExampleTest.copyFile("ForCopyTest_Error", "ForCopyTest_copy");
        Assertions.assertFalse(excepted);
    }

    /**
     * Проверка метода copyBufferedFile
     */
    @Test
    void copyBufferedFileTest() {
        boolean excepted = myIOExampleTest.copyBufferedFile("ForCopyTest", "ForCopyTest_copyBuff");
        Assertions.assertTrue(excepted);
    }

    /**
     * Проверка метода copyBufferedFile при попытке скопировать несуществующий файл
     */
    @Test
    void copyBufferedFileTest_Error() {
        boolean excepted = myIOExampleTest.copyBufferedFile("ForCopyTest_Error", "ForCopyTest_copyBuff");
        Assertions.assertFalse(excepted);
    }

    /**
     * Проверка метода opyFileWithReaderAndWrite
     */
    @Test
    void copyFileWithReaderAndWriteTest() {
        boolean excepted = myIOExampleTest.copyFileWithReaderAndWriter("ForCopyTest", "ForCopyTest_copyRW");
        Assertions.assertTrue(excepted);
    }

    /**
     * Проверка метода opyFileWithReaderAndWrite при попытке скопировать несуществующий файл
     */
    @Test
    void copyFileWithReaderAndWriteTest_Error() {
        boolean excepted = myIOExampleTest.copyFileWithReaderAndWriter("ForCopyTest_Error", "ForCopyTest_copyRW");
        Assertions.assertFalse(excepted);
    }

}