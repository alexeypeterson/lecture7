
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sbp.JDBC.Person;
import sbp.db.PersonDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


class PersonDaoTest {

    /**
     * Очищаем таблицу перед каждыйм тестом
     */
    @BeforeEach
    public void beforeEachDelAll(){
        final String createPersonPreparedStatement = "delete from Person";

        String LOCAL_URL = "jdbc:sqlite:F:\\JDSber\\DB\\JDSber.db";
        try (Connection connection = DriverManager.getConnection(LOCAL_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(createPersonPreparedStatement))
        {
            preparedStatement.executeUpdate();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }


    PersonDao daoTest = new PersonDao();

    /**
     * Проверка метод createPerson
     * успешное создание записи в таблице Person
     */
    @Test
    void createPersonTest() {
        boolean excepted = daoTest.createPerson(new Person("Alexey", "Ekb", 29));
        Assertions.assertTrue(excepted);
    }

    /**
     * Проверка метод readAllPerson
     * выборка всех записей из таблицы Person
     */
    @Test
    void readAllPersonTest() {
        Person person = new Person("Filip","Ketovo", 29);
        Person person2 = new Person("Anton","Msk", 21);
        Person person3 = new Person("Egor","Spb", 23);

        daoTest.createPerson(person);
        daoTest.createPerson(person2);
        daoTest.createPerson(person3);
        List<Person> stringListTest = new ArrayList<>();
        Assertions.assertEquals(0, stringListTest.size());
        stringListTest = daoTest.readAllPerson();
        Assertions.assertEquals(3, stringListTest.size());

    }

    /**
     * Проверка метода deletePerson
     * удаляет строку из таблицы Person
     */
    @Test
    void deletePersonTest() {
        Person person = new Person("Filip","Ketovo", 29);
        Person person2 = new Person("Anton","Msk", 21);
        Person person3 = new Person("Egor","Spb", 23);

        daoTest.createPerson(person);
        daoTest.createPerson(person2);
        daoTest.createPerson(person3);
        List<Person> stringListTest = new ArrayList<>();
        Assertions.assertEquals(0, stringListTest.size());
        daoTest.deletePerson(daoTest.getIDbyPerson("Egor","Spb", 23));
        stringListTest = daoTest.readAllPerson();
        Assertions.assertEquals(2, stringListTest.size());
    }

    /**
     * Проверка метода readPersonByName
     * возвращает список пользователей с указанным именем
     */
    @Test
    void readPersonByNameTest() {
        Person person = new Person("Filip","Ketovo", 29);
        Person person2 = new Person("Anton","Msk", 21);
        Person person3 = new Person("Egor","Spb", 23);

        daoTest.createPerson(person);
        daoTest.createPerson(person2);
        daoTest.createPerson(person3);
        List<Person> stringListTest = new ArrayList<>();
        Assertions.assertEquals(0, stringListTest.size());
        stringListTest = daoTest.readPersonByName("Anton");
        Assertions.assertEquals(1, stringListTest.size());
        Assertions.assertEquals(person2,stringListTest.get(0));
    }

    /**
     * Проверка метода readPersonByCity
     * возвращает список пользователей с указанным городом
     */
    @Test
    void readPersonByCityTest() {
        Person person = new Person("Filip","Ketovo", 29);
        Person person2 = new Person("Anton","Msk", 21);
        Person person3 = new Person("Egor","Ketovo", 23);

        daoTest.createPerson(person);
        daoTest.createPerson(person2);
        daoTest.createPerson(person3);
        List<Person> stringListTest = new ArrayList<>();
        Assertions.assertEquals(0, stringListTest.size());
        stringListTest = daoTest.readPersonByCity("Ketovo");
        Assertions.assertEquals(2, stringListTest.size());
        Assertions.assertEquals(person,stringListTest.get(0));
        Assertions.assertEquals(person3,stringListTest.get(1));

    }
}