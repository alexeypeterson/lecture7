package sbp.controller;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import sbp.JDBC.Person;
import sbp.db.PersonDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@RunWith(MockitoJUnitRunner.class)
class ClientControllerTest {


    PersonDao daoTest = new PersonDao();
    ClientController controllerTest = new ClientController(daoTest);

    @InjectMocks
    private ClientController controller = new ClientController(daoTest);

    /**
     * Очищаем таблицу перед каждым тестом
     */
    @BeforeEach
    public void beforeEachDelAll(){
        final String createPersonPreparedStatement = "delete from Person";

        String LOCAL_URL = "jdbc:sqlite:F:\\JDSber\\DB\\JDSber.db";
        try (Connection connection = DriverManager.getConnection(LOCAL_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(createPersonPreparedStatement))
        {
            preparedStatement.executeUpdate();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Проверка createPerson при успешном выполнении
     */
    @Test
    void createPersonTest() {
        Person personTest = new Person("Alexey", "Ekb", 29);
        ResponseEntity<?> response = controllerTest.createPerson(personTest);
        ResponseEntity<Object> excepted = new ResponseEntity<>(HttpStatus.CREATED);
        Assertions.assertEquals(excepted, response);
    }

    /**
     * Проверка readPerson при успешном выполнении
     */
    @Test
    void readPersonTest() {
        Person personTest = new Person("Alexey", "Ekb", 29);
        controllerTest.createPerson(personTest);

        ResponseEntity<?> response = controllerTest.readPerson();
        ResponseEntity<Object> excepted = new ResponseEntity<>(HttpStatus.OK);
        Assertions.assertEquals(excepted, response);
    }

    /**
     * Проверка readByName при успешном выполнении
     */
    @Test
    void readByNameTest() {
        Person personTest = new Person("Alexey", "Ekb", 29);
        controllerTest.createPerson(personTest);
        ResponseEntity<?> response = controllerTest.readByName("Alexey");
        ResponseEntity<Object> excepted = new ResponseEntity<>(HttpStatus.CREATED);
        Assertions.assertEquals(excepted, response);
    }

    /**
     * Проверка updateByName при успешном выполнении
     */
    @Test
    void updateByNameTest() {
        Person personTest = new Person("Alexey", "Ekb", 29);
        controllerTest.createPerson(personTest);

        PersonDao daoTest = new PersonDao();
        int idUpdate = daoTest.getIDbyPerson("Alexey", "Ekb", 29);

        ResponseEntity<?> response = controllerTest.updateByName(idUpdate,"AlexeyChange", "EkbChange", 12);
        ResponseEntity<Object> excepted = new ResponseEntity<>(HttpStatus.OK);
        Assertions.assertEquals(excepted, response);
    }

    /**
     * Проверка deleteByID, при успешном удалении пользователя из таблицы Person
     * daoTest.getIDbyPerson - получение ID записи по полному совпадению параметров
     */
    @Test
    void deleteByIDTest() {
        Person personTest = new Person("Alexey", "Ekb", 29);
        controllerTest.createPerson(personTest);

        PersonDao daoTest = new PersonDao();
        int idDelete = daoTest.getIDbyPerson("Alexey", "Ekb", 29);

        ResponseEntity<?> response = controllerTest.deleteByID(idDelete);
        ResponseEntity<Object> excepted = new ResponseEntity<>(HttpStatus.OK);
        Assertions.assertEquals(excepted, response);
    }



}